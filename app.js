const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');

const spanFullName = document.querySelector('#span-full-name');

/* 
ACTIVITY: 

>> Copy the index.html and index.js to the activity folder.

>> Listen to an event when the last name's input is changed.

>> Instead of anonymous functions for each of the event listener:
    >> Create a function that will update the span's contents based on the value of the first and last name input fields.
    >> Instruct the event listeners to use the created function.
*/

//! Solution #1
// let arr = [];

// txtFirstName.addEventListener('keyup', (event) => {
//   arr.push(txtFirstName.value);
// });

// //multiple listeners
// txtLastName.addEventListener('keyup', (event) => {
//   spanFullName.innerHTML = arr[arr.length - 1] + ' ' + txtLastName.value;
// });

//! Solution #2
// txtFirstName.addEventListener &&
//   txtLastName.addEventListener('keyup', (event) => {
//     spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
//   });

/* 
  CALLBACK
    A callback is a function passed as an argument to another function

    This technique allows a function to call another function

    A callback function can run after another function has finished
*/
//! Solution #3
//https://discuss.codecademy.com/t/combining-two-event-listeners-for-one-result/545627
//https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
// let joinName = () => {
//   spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
// };

// txtFirstName.addEventListener('keyup', joinName);
// txtLastName.addEventListener('keyup', joinName);

const keyCodeEvent = (e) => {
  e.key.toLowerCase() === 'w' && console.log('move forward');
};

txtFirstName.addEventListener('keyup', keyCodeEvent);
